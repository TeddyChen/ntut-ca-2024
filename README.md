This repository contains code for the software architecture course at NTUT, 2024.
We will refactor the code by applying Clean Architecture-Driven Refactoring.

Clone from: 
https://github.com/codurance/task-list
