package com.codurance.training.tasks.adapter.controller;

import com.codurance.training.tasks.TaskList;
import com.codurance.training.tasks.adapter.presenter.HelpConsolePresenter;
import com.codurance.training.tasks.adapter.presenter.ShowConsolePresenter;
import com.codurance.training.tasks.entity.ToDoList;
import com.codurance.training.tasks.usecase.Add;
import com.codurance.training.tasks.usecase.Error;
import com.codurance.training.tasks.usecase.SetDone;
import com.codurance.training.tasks.usecase.port.in.project.add.AddProjectInput;
import com.codurance.training.tasks.usecase.port.in.todolist.help.HelpUseCase;
import com.codurance.training.tasks.usecase.port.in.todolist.show.ShowInput;
import com.codurance.training.tasks.usecase.port.in.todolist.show.ShowOutput;
import com.codurance.training.tasks.usecase.port.in.todolist.show.ShowUseCase;
import com.codurance.training.tasks.usecase.port.out.ToDoListRepository;
import com.codurance.training.tasks.usecase.port.out.todolist.show.ShowPresenter;
import com.codurance.training.tasks.usecase.service.AddProjectService;
import com.codurance.training.tasks.usecase.service.HelpService;
import com.codurance.training.tasks.usecase.service.ShowService;
import tw.teddysoft.ezddd.core.usecase.Input;

import java.io.PrintWriter;

public class Execute {
    private final ToDoList toDoList;
    private final PrintWriter out;
    private final ToDoListRepository repository;

    public Execute(ToDoList toDoList, PrintWriter out, ToDoListRepository repository) {
        this.toDoList = toDoList;
        this.out = out;
        this.repository = repository;
    }

    public void execute(String commandLine) {
        String[] commandRest = commandLine.split(" ", 2);
        String command = commandRest[0];
        switch (command) {
            case "show":
                ShowUseCase showUseCase = new ShowService(repository);
                ShowInput showInput = new ShowInput();
                showInput.toDoListId = TaskList.DEFAULT_TO_DO_LIST_ID.value();
                ShowOutput output = showUseCase.execute(showInput);
                ShowPresenter presenter = new ShowConsolePresenter(out);
                presenter.present(output.toDoListDto);
                break;
            case "add":
                String[] subcommandRest = commandRest[1].split(" ", 2);
                String subcommand = subcommandRest[0];
                if (subcommand.equals("project")) {
                    AddProjectInput input = new AddProjectInput();
                    input.toDoListId = TaskList.DEFAULT_TO_DO_LIST_ID.value();
                    input.projectName = subcommandRest[1];
                    new AddProjectService(repository).execute(input);
                } else if (subcommand.equals("task")) {
                    new Add(toDoList, out).add(commandRest[1]);
                }
                break;
            case "check":
                new SetDone(toDoList, out).setDone(commandRest[1], true);
                break;
            case "uncheck":
                new SetDone(toDoList, out).setDone(commandRest[1], false);
                break;
            case "help":
                //new Help(out).help();
                HelpUseCase helpUseCase = new HelpService(new HelpConsolePresenter(out));
                helpUseCase.execute(new Input.NullInput());
                break;
            default:
                new Error(out).error(command);
                break;
        }
    }
}
