package com.codurance.training.tasks.adapter.presenter;

import com.codurance.training.tasks.usecase.port.in.todolist.show.ProjectDto;
import com.codurance.training.tasks.usecase.port.in.todolist.show.TaskDto;
import com.codurance.training.tasks.usecase.port.in.todolist.show.ToDoListDto;
import com.codurance.training.tasks.usecase.port.out.todolist.show.ShowPresenter;

import java.io.PrintWriter;

public class ShowConsolePresenter implements ShowPresenter {

    private final PrintWriter out;

    public ShowConsolePresenter(PrintWriter out) {
        this.out = out;
    }

    @Override
    public void present(ToDoListDto toDoListDto) {
        for (ProjectDto project : toDoListDto.projectDots)
        {
            out.println(project.name);
            for (TaskDto task : project.taskDtos) {
                out.printf("    [%c] %s: %s%n", (task.done? 'x' : ' '), task.id, task.description);
            }
            out.println();
        }
    }
}
