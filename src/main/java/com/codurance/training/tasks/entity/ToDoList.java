package com.codurance.training.tasks.entity;

import tw.teddysoft.ezddd.core.entity.AggregateRoot;
import tw.teddysoft.ezddd.core.entity.DomainEvent;

import java.util.*;

public class ToDoList extends AggregateRoot<ToDoListId, DomainEvent> {

    private final ToDoListId id;
    private final List<Project> projects;

    private long lastId = 0;

    public ToDoList(ToDoListId id) {
        this.id = id;
        this.projects = new ArrayList<>();
    }

    public boolean containsProject(ProjectName projectName){
        return projects.stream()
                        .filter(project -> project.getName().equals(projectName))
                        .findFirst().isPresent();
    }
    public void addTask(ProjectName projectName, TaskId taskId, String description, boolean done){
        Project project =
                projects.stream()
                        .filter(p -> p.getName().equals(projectName))
                        .findFirst().get();

        project.addTask(taskId, description, done);
    }

    public List<Project> getProjects() {
        return projects.stream()
                .map(p -> (Project) new ReadOnlyProject(p.getName(), p.getTasks()))
                .toList();
    }

    public void addProject(ProjectName name, ArrayList<Task> tasks) {
        Project project = new Project(name, tasks);
        this.projects.add(project);
    }

    public List<Task> getTasks(ProjectName projectName) {
        Optional<Project> p =
         projects.stream()
                .filter(project -> project.getName().equals(projectName))
                .findFirst();
        if(p.isEmpty()){
            return null;
        }

        return p.get().getTasks().stream()
                .map(t -> (Task) new ReadOnlyTask(t.getId(), t.getDescription(), t.isDone()))
                .toList();
    }

    @Override
    public ToDoListId getId() {
        return id;
    }

    public long nextId() {
        return ++lastId;
    }
}
