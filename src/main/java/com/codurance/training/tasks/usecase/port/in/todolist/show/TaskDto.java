package com.codurance.training.tasks.usecase.port.in.todolist.show;

public class TaskDto {
    public String id;
    public String description;
    public boolean done;
}
