package com.codurance.training.tasks.usecase;

import com.codurance.training.tasks.entity.ProjectName;
import com.codurance.training.tasks.entity.Task;
import com.codurance.training.tasks.entity.TaskId;
import com.codurance.training.tasks.entity.ToDoList;

import java.io.PrintWriter;
import java.util.ArrayList;

public class Add {

    private final ToDoList toDoList;
    private final PrintWriter out;

    public Add(ToDoList toDoList, PrintWriter out) {
        this.toDoList = toDoList;
        this.out = out;
    }

    public void add(String commandLine) {
        String[] subcommandRest = commandLine.split(" ", 2);
        String subcommand = subcommandRest[0];
        if (subcommand.equals("project")) {
            addProject(subcommandRest[1]);
        } else if (subcommand.equals("task")) {
            String[] projectTask = subcommandRest[1].split(" ", 2);
            addTask(projectTask[0], projectTask[1]);
        }
    }

    private void addProject(String name) {
        toDoList.addProject(ProjectName.of(name), new ArrayList<Task>());
    }

    private void addTask(String name, String description) {
        var projectName = ProjectName.of(name);
        if (!toDoList.containsProject(projectName)) {
            out.printf("Could not find a project with the name \"%s\".", name);
            out.println();
            return;
        }
        toDoList.addTask(projectName, TaskId.of(toDoList.nextId()), description, false);
    }
}
