package com.codurance.training.tasks.usecase.port.out;

import com.codurance.training.tasks.entity.ToDoList;

import java.util.Optional;

public interface ToDoListRepository<ID> {

    Optional<ToDoList> findById(ID id);
    void save(ToDoList toDoList);
}
