package com.codurance.training.tasks.usecase;

import com.codurance.training.tasks.entity.ToDoList;
import com.codurance.training.tasks.entity.ToDoListId;
import com.codurance.training.tasks.usecase.port.out.ToDoListRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ToDoListInMemoryRepository implements ToDoListRepository<ToDoListId> {

    private final List<ToDoList> store = new ArrayList<>();


    @Override
    public Optional<ToDoList> findById(ToDoListId toDoListId) {
        return store.stream().filter( x -> x.getId().equals(toDoListId)).findFirst();
    }

    @Override
    public void save(ToDoList toDoList) {
        store.remove(toDoList);
        store.add(toDoList);
    }
}
