package com.codurance.training.tasks.usecase.service;

import com.codurance.training.tasks.entity.ToDoList;
import com.codurance.training.tasks.usecase.port.ToDoListMapper;
import com.codurance.training.tasks.entity.ToDoListId;
import com.codurance.training.tasks.usecase.port.out.ToDoListRepository;
import com.codurance.training.tasks.usecase.port.in.todolist.show.ShowInput;
import com.codurance.training.tasks.usecase.port.in.todolist.show.ShowOutput;
import com.codurance.training.tasks.usecase.port.in.todolist.show.ShowUseCase;

public class ShowService implements ShowUseCase {
    private final ToDoListRepository repository;
    public ShowService(ToDoListRepository repository) {
        this.repository = repository;
    }

    @Override
    public ShowOutput execute(ShowInput input) {
        ToDoList toDoList = (ToDoList) repository.findById(ToDoListId.of(input.toDoListId)).get();
        ShowOutput output = new ShowOutput();
        output.toDoListDto = ToDoListMapper.toDto(toDoList);
        return output;
    }
}
