package com.codurance.training.tasks.usecase.service;

import com.codurance.training.tasks.entity.ProjectName;
import com.codurance.training.tasks.entity.Task;
import com.codurance.training.tasks.entity.ToDoList;
import com.codurance.training.tasks.entity.ToDoListId;
import com.codurance.training.tasks.usecase.port.out.ToDoListRepository;
import com.codurance.training.tasks.usecase.port.in.project.add.AddProjectInput;
import com.codurance.training.tasks.usecase.port.in.project.add.AddProjectUseCase;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

import java.util.ArrayList;
import java.util.Optional;

public class AddProjectService implements AddProjectUseCase {
    private final ToDoListRepository repository;
    public AddProjectService(ToDoListRepository repository) {
        this.repository = repository;
    }

    @Override
    public CqrsOutput execute(AddProjectInput projectInput) {

        Optional<ToDoList> toDoList = repository.findById(ToDoListId.of(projectInput.toDoListId));
        toDoList.get().addProject(ProjectName.of(projectInput.projectName), new ArrayList<Task>());
        repository.save(toDoList.get());

        return CqrsOutput.create().succeed().setId(projectInput.projectName);
    }

}
