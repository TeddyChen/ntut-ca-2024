package com.codurance.training.tasks.usecase.port.in.todolist.show;

import java.util.ArrayList;
import java.util.List;

public class ToDoListDto {
    public String id;
    public List<ProjectDto> projectDots;
    public ToDoListDto() {
        this.projectDots = new ArrayList<>();
    }
}
