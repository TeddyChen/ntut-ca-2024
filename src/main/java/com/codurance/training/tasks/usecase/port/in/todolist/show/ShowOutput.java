package com.codurance.training.tasks.usecase.port.in.todolist.show;

import com.codurance.training.tasks.entity.ToDoList;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public class ShowOutput extends CqrsOutput<ShowOutput> {
    public ToDoListDto toDoListDto;
}
