package com.codurance.training.tasks.usecase.port.in.todolist.show;

import java.util.List;

public class ProjectDto {
    public String name;
    public List<TaskDto> taskDtos;
}
