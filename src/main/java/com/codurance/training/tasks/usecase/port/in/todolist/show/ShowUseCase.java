package com.codurance.training.tasks.usecase.port.in.todolist.show;

import tw.teddysoft.ezddd.core.usecase.Input;
import tw.teddysoft.ezddd.cqrs.usecase.query.Query;

public interface ShowUseCase
        extends Query<ShowInput, ShowOutput> {
}
