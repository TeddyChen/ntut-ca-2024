package com.codurance.training.tasks.usecase;

import com.codurance.training.tasks.entity.Task;
import com.codurance.training.tasks.entity.TaskId;
import com.codurance.training.tasks.entity.ToDoList;

import java.io.PrintWriter;

public class SetDone {
    private final ToDoList toDoList;
    private final PrintWriter out;

    public SetDone(ToDoList toDoList, PrintWriter out) {
        this.toDoList = toDoList;
        this.out = out;
    }

    public void setDone(String idString, boolean done) {
        TaskId id = TaskId.of(idString);
        for (var project : toDoList.getProjects()) {
            for (Task task : project.getTasks()) {
                if (task.getId().equals(id)) {
                    task.setDone(done);
                    return;
                }
            }
        }
        out.printf("Could not find a task with an ID of %s.", id);
        out.println();
    }

}
