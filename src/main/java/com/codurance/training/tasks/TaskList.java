package com.codurance.training.tasks;

import com.codurance.training.tasks.adapter.controller.Execute;
import com.codurance.training.tasks.adapter.presenter.HelpConsolePresenter;
import com.codurance.training.tasks.adapter.presenter.ShowConsolePresenter;
import com.codurance.training.tasks.entity.*;
import com.codurance.training.tasks.usecase.*;
import com.codurance.training.tasks.usecase.Error;
import com.codurance.training.tasks.usecase.port.in.project.add.AddProjectInput;
import com.codurance.training.tasks.usecase.port.in.todolist.help.HelpUseCase;
import com.codurance.training.tasks.usecase.port.in.todolist.show.*;
import com.codurance.training.tasks.usecase.port.out.ToDoListRepository;
import com.codurance.training.tasks.usecase.port.out.todolist.show.ShowPresenter;
import com.codurance.training.tasks.usecase.service.AddProjectService;
import com.codurance.training.tasks.usecase.service.HelpService;
import com.codurance.training.tasks.usecase.service.ShowService;
import tw.teddysoft.ezddd.core.usecase.Input;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public final class TaskList implements Runnable {
    private static final String QUIT = "quit";
    public static final ToDoListId DEFAULT_TO_DO_LIST_ID = ToDoListId.of("001");

//    private final Map<String, List<Task>> tasks = new LinkedHashMap<>();
    public final static ToDoList toDoList = new ToDoList(DEFAULT_TO_DO_LIST_ID);

    private final ToDoListRepository repository;
    private final BufferedReader in;
    private final PrintWriter out;



    public static void main(String[] args) throws Exception {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter out = new PrintWriter(System.out);
        ToDoListRepository repository = new ToDoListInMemoryRepository();
//        ToDoList toDoList = new ToDoList(DEFAULT_TO_DO_LIST_ID);
        repository.save(toDoList);
        new TaskList(in, out, repository).run();
    }

    public TaskList(BufferedReader reader, PrintWriter writer, ToDoListRepository repository) {
        this.in = reader;
        this.out = writer;
        this.repository = repository;
    }

    public void run() {
        while (true) {
            out.print("> ");
            out.flush();
            String command;
            try {
                command = in.readLine();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            if (command.equals(QUIT)) {
                break;
            }
            new Execute(toDoList, out, repository).execute(command);
        }
    }


}
